Boa tarde, este ReadMe traz algumas informações sobre o projeto, que foi feito na Unity 2020.3.1.f1.
Subi tudo em um repositório que está com acesso público, caso queiram acompanhar os commits e verificar o andamento, dias, prazos, etc, nos seguintes links:

https://Nosfera@bitbucket.org/Nosfera/wildlifearttest.git
git@bitbucket.org:Nosfera/wildlifearttest.git

Sobre as construções existem duas pastas de prefabs. Na raiz "Buildings" estão os prefabs das construções feitas a partir dos prefabs das partes isoladas que estão dentro de "Parts".
Existe um plugin para contar os vertices da cena, pois a interpretação da Unity é diferente dos softwares de modelagem e não exceder o limite do teste de 2500 vertices seria difícil somente pelo contador de vertex da unity, na janela Stats.
Isto porque a Unity intepreta luzes, UV's e normais para calcular a quantidade de vértices na cena, o que quer dizer que um cubo que no Maya3D por exemplo possui 8 vértices na cena apareça com 12. O efeito exponencial de vários planes, cubes e cylinders na cena facilmente extrapolaria o limite de 2500 e montar toda cena no softwares 3D não geraria os módulos necessário para o teste.

Acrescentei sons e animações para complementar a ideia da cena, que se trata de um crime recém cometido com a polícia investigando e um helicóptero em busca do suspeito.
A luz do poste piscando indica suspense e paranormalidade à cena.

Desde já agradeço a oportunidade pela vaga.

At.te,
Artur Júnior


